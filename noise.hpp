#pragma once

#include <lux_shared/int.hpp>
#include <lux_shared/vec.hpp>

F32 noise(F32 seed);
F32 noise(Vec2F seed);
F32 noise(Vec3F seed);
Vec2F noise_deriv(F32 seed);
Vec3F noise_deriv(Vec2F seed);
Vec4F noise_deriv(Vec3F seed);

template<typename T>
F32 noise_fbm(T seed, Uns octaves);

template<typename T1, typename T2>
T1 noise_deriv_fbm(T2 seed, Uns octaves);
